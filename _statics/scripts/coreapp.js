var coreapp = coreapp || {};

//Configuracion general de este modulo
coreapp.config = {
	csrf: null,
	api_url: 'http://localhost:8000/api/koala/v1/',
};
coreapp.saveToStorage = function(id, data) {
	try {
		data.timeStamp = new Date().getTime();
		localStorage[id] = JSON.stringify(data);
	} catch (error) {
		console.log(error);
	}
};
coreapp.fetchFromStorage = function(id, fresh) {
	try {
		if (localStorage[id]) {
			var lsData = JSON.parse(localStorage[id]);
			if (fresh) {
				if (Number(lsData.timeStamp) + coreapp.config.freshDataMargin >= (new Date()).getTime())
					return lsData;
			} else{
				return lsData;
			}
		}
		return null;
	} catch (error) {
		console.log(error);
	}
};
coreapp.removeFromStorage = function(id) {
	try {
		if (localStorage[id]) {
			localStorage.removeItem(id);
		}
	} catch (error) {
		console.log(error);
	}
};
coreapp.clearStorage = function() {
	localStorage.clear();
	location.reload();
};
coreapp.csrfSafeMethod = function(method) {
	return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
coreapp.getCookie = function(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
coreapp.getCsrfToken = function(successResponse) {
	try{
		token = $('input[name="csrfmiddlewaretoken"]').val()
		coreapp.config.csrf = token;
		if(successResponse) successResponse()
	} catch(error){

	}
}
coreapp.ajaxSetup = function() {
	$.ajaxSetup({
	    headers: {
	    	"X-CSRFToken": coreapp.config.csrf
	    }
	});
}
coreapp.templateReplace = function(template,data){
	return template.replace(/{([^}]+)}/g,function(match,group){
		return data[group.toLowerCase()];
	});
};

coreapp.serializeJsonData = function(data){
	serialize_data = '';
	$.each(data, function(key, value){
		if(serialize_data === ''){
			serialize_data = key + '=' + value;
		} else {
			serialize_data += '&' + key + '=' + value;
		}
	});
	return serialize_data;
};
coreapp.postRequest = function(type, operation, sendData, successCallback, errorCallBack) {
	try {
		$.ajax({
			type: type.toUpperCase(),
			url: coreapp.config.api_url + operation,
			data: sendData,
			dataType: 'json',
			cache: false,
			beforeSend:function(xhr){
			},
			success: function(data) {
				try {
					if (successCallback) successCallback(data);
				} catch (error) {
					console.log("Error_catch",error);
				}
			},
			error: function(data) {
				console.log(data);
				if (errorCallBack) errorCallBack(data);
			},
			complete: function(){
			}
		});
	} catch (error) {
		console.log("Error_catch",error);
	}
};
coreapp.stopLoader = function() {
	$('.theme-loader').fadeOut('slow', function() {
        $(this).remove();
    });
}
coreapp.notificate = function(icon, type, title, message) {
    $.growl({
        icon: icon,
        title: title,
        message: message,
        url: ''
    },{
        element: 'body',
        type: type,
        allow_dismiss: true,
        placement: {
            from: 'top',
            align: 'right'
        },
        offset: {
            x: 30,
            y: 30
        },
        spacing: 10,
        z_index: 999999,
        delay: 2500,
        timer: 1000,
        url_target: '_blank',
        mouse_over: false,
        icon_type: 'class',
        template: '<div data-growl="container" class="alert" role="alert">' +
        '<button type="button" class="close" data-growl="dismiss">' +
        '<span aria-hidden="true">&times;</span>' +
        '<span class="sr-only">Close</span>' +
        '</button>' +
        '<span data-growl="icon"></span>' +
        '<span data-growl="title"></span>' +
        '<span data-growl="message"></span>' +
        '<a href="#" data-growl="url"></a>' +
        '</div>'
    });
}
coreapp.startPCode = function() {
	$("#pcoded").pcodedmenu({
        themelayout: 'vertical',
        verticalMenuplacement: 'left',
        verticalMenulayout: 'wide',
        MenuTrigger: 'click', 
        SubMenuTrigger: 'click',
        activeMenuClass: 'active',
        ThemeBackgroundPattern: 'pattern4',  // pattern1, pattern2, pattern3, pattern4, pattern5, pattern6
        HeaderBackground: 'theme1',  // theme1, theme2, theme3, theme4, theme5  header color
        LHeaderBackground: 'theme1', // theme1, theme2, theme3, theme4, theme5, theme6   brand color
        NavbarBackground: 'themelight1', // themelight1, theme1  // light  and dark sidebar
        ActiveItemBackground: 'theme4', // theme1, theme2, theme3, theme4, theme5, theme6, theme7, theme8, theme9, theme10, theme11, theme12  mennu active item color
        SubItemBackground: 'theme2',
        ActiveItemStyle: 'style0',
        ItemBorder: true,
        ItemBorderStyle: 'none',
        SubItemBorder: true,
        DropDownIconStyle: 'style1', // Value should be style1,style2,style3
        menutype: 'st5', // Value should be st1, st2, st3, st4, st5 menu icon style
        freamtype: "theme1",
        layouttype:'light', // Value should be light / dark
        FixedNavbarPosition: true,  // Value should be true / false  header postion
        FixedHeaderPosition: true,  // Value should be true / false  sidebar menu postion
        collapseVerticalLeftHeader: true,
        VerticalSubMenuItemIconStyle: 'style6', // value should be style1, style2, style3, style4, style5, style6
        VerticalNavigationView: 'view1',
        verticalMenueffect: {desktop: "shrink", tablet: "overlay", phone: "overlay",},
        defaultVerticalMenu: {desktop: "expanded", tablet: "offcanvas", phone: "offcanvas",},
        onToggleVerticalMenu: {desktop: "offcanvas", tablet: "expanded",  phone: "expanded",},

    });
}
coreapp.addRegexValidator = function() {
	$.validator.addMethod("regx", function(value, element, regexpr) {          
    	return regexpr.test(value);
	}, "Incorrect information.");
}
coreapp.startApp = function() {
	coreapp.startPCode();
	coreapp.stopLoader();
}
$(document).ready(function() {
	coreapp.getCsrfToken(function(){
		coreapp.ajaxSetup()
		coreapp.startApp();
	});
});