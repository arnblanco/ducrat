var ducrat = ducrat || {};
ducrat.config = {
	feed: {
		chanel: {},
		media_photo: 0
	},
	articles: {
		chanel: {}
	},
	uploader: {
		action: '',
		image: ''
	}
}
//DUCRAT COMMENTS
ducrat.socialFeedUploadPhoto = function($inputImage) {
	$inputImage.change(function() {
	    var fileReader = new FileReader(),
	            files = this.files,
	            file,
	            content_image = ducrat.socialFeedNewImage();

	    if(ducrat.config.feed.media_photo == 3) $inputImage.prop('disabled', true);

	    if (!files.length) {
	        return;
	    }

	    file = files[0];

	    if (/^image\/\w+$/.test(file.type)) {
	        fileReader.readAsDataURL(file);
	        fileReader.onload = function () {
	            $inputImage.val("");
	            content_image.children('.image-content').html('<input type="hidden" name="post-gallery[]" value="'+this.result+'" />').css('background-image', 'url("'+this.result+'")');
	        };
	    } else {
	    	toastr.error('Wrong file, select an image.');
	    }
	});
};
ducrat.socialFeedNewImage = function() {
	to_add_elements = $('#social-feed-media-content');
	if(ducrat.config.feed.media_photo === 0) to_add_elements.show();

	ducrat.config.feed.media_photo++;
	image_object = $('<div class="feed-thumbnail-imagecontainer col-md-4" id="Image-'+ducrat.config.feed.media_photo+'"><div class="feed-drop-image" cursor="'+ducrat.config.feed.media_photo+'"><i class="fas fa-trash-alt"></i></div><div class="image-content"></div></div>');
	to_add_elements.append(image_object);
	ducrat.socialFeedDeleteImage();
	return image_object;
};
ducrat.socialFeedDeleteImage = function() {
	$('.feed-drop-image').unbind('click');
	$('.feed-drop-image').on('click', function(){
		cursor = $(this).attr('cursor');
		image_element = $('#Image-' + cursor);
		image_element.remove();
		if(ducrat.config.feed.media_photo == 3) $('#user-upload-photo').prop('disabled', false);
		ducrat.config.feed.media_photo--;
		if(ducrat.config.feed.media_photo == 0) $('#social-feed-media-content').hide();
	})
};
ducrat.socialFeedLikeAction = function() {
	$('.feed-post-action-like').unbind('click');
	$('.feed-post-action-like').on('click', function(e){
		e.preventDefault();

		var me_element = $(this);
		coreapp.postRequest('POST', 'post/like', {'post':me_element.attr('post')},
		function(response){
			if(response.error) {
				toastr.error(response.message)
			} else {
				if(response.action) {
					me_element.addClass('i-like-this-post');
				} else {
					me_element.removeClass('i-like-this-post');
				}
			}
		}, function(error){
			console.log('error', error);
		});
	});
};
ducrat.socialFeedSharePost = function() {
	$('.feed-post-action-share').unbind('click');
	$('.feed-post-action-share').on('click', function(e){
		e.preventDefault();

		var me_element = $(this);
		coreapp.postRequest('GET', 'post/share', {'post':me_element.attr('post')},
		function(response){
			if(response.error) {
				toastr.erro(response.message);
			} else {
				comment_textarea = $('#feed-post-textarea-share');
				//autosize(comment_textarea);
				$('#feed-share-post-subpost').html(response.comment);
				$('#social-feed-share').modal('show');
				ducrat.socialFeedSharePostAction(me_element.attr('post'), comment_textarea);
			}
		}, function(error){
			console.log(error);
		});
	});
};
ducrat.socialFeedSharePostAction = function(post, text){
	$('#social-feed-share-post').unbind('click');
	$('#social-feed-share-post').on('click', function(){
		var b_spin = $(this).ladda();
		b_spin.ladda('start');
		try {
			coreapp.postRequest('POST', 'post/share', {'comment':text.val(), 'post':post},
			function(response){
				b_spin.ladda('stop');
				if(response.error) {
					toastr.error(response.message);
				} else {
					if(ducrat.config.feed.chanel === 'profile-me') {
						$('#social-feed-post-content').prepend(response.comment);
						ducrat.socialFeedPostActions();
					}
					
					$('#feed-share-post-subpost').html('');
					$('#social-feed-share').modal('hide');
				}
			}, function(error){
				b_spin.ladda('stop');
				console.log(error);
			});
		} catch(error) {
			console.log(error);
			b_spin.ladda('stop');
		}
	});
};
ducrat.socialFeedPostActions = function() {
	ducrat.socialFeedLikeAction();
	ducrat.socialFeedSharePost();
};
ducrat.socialFeedLoadPost = function() {
	coreapp.postRequest('GET', 'post', ducrat.config.feed.chanel,
	function(response){
		if(response.error) {

		} else {
			response_elements = $(response.comment);
			$('#social-feed-post-content').append(response_elements);
			ducrat.socialFeedPostActions();
			ducrat.socialFeedPagination();
		}
	}, function(error){
		console.log('error', error);
	});
};
ducrat.socialFeedPagination = function() {
	console.log('Start Pagination');

	$(window).scroll(function(e) {
		
	});
};
ducrat.socialFeedPost = function(){
	$('#social-feed-post').on('click', function(){
		var b_spin = $(this).ladda(),
			b_content = $('#social-feed-box');

		if($('#social-feed-textarea').val() == '' && ducrat.config.feed.media_photo == 0) {
			console.log('NoComment', $('#social-feed-textarea').val());
			return false;
		}

		b_spin.ladda('start');
		b_content.toggleClass('card-load');

		try {
			post_data = $('#social-feed-form').serialize();
			post_data += '&' + coreapp.serializeJsonData(ducrat.config.feed.chanel);
			coreapp.postRequest('POST', 'post', post_data,
			function(response){
				$('#social-feed-textarea').val('');
				$('#social-feed-media-content').html('').hide();
				ducrat.config.feed.media_photo = 0;
				b_spin.ladda('stop');
				b_content.toggleClass('card-load');

				if(response.error) {
					toastr.error(response.message);
				} else {
					$('#social-feed-post-content').prepend(response.comment);
					ducrat.socialFeedPostActions();
				}
			}, function(error){
				console.log(error);
				b_spin.ladda('stop');
				b_content.toggleClass('card-load');
			});
		} catch(error) {
			console.log(error);
			b_spin.ladda('stop');
			b_content.toggleClass('card-load');
		}
	});
};
//DUCRAT PROFILE
ducrat.profileUserFriendship = function() {
	$('.proc-users-follow').unbind('click');
	$('.proc-users-follow').unbind('hover');

	$('.proc-users-follow').on('click', function(e){
		e.preventDefault();
		var me_element = $(this),
			b_spin = me_element.ladda();

		b_spin.ladda('start');
		coreapp.postRequest('POST', 'me/action/friendship', {user:me_element.attr('username')},
		function(response){
			b_spin.ladda('stop');
			if(response.error) {
				toastr.error(response.message)
			} else {
				if(response.action) {
					me_element.addClass('is-follow');
					me_element.html(me_element.attr('following-message'));
					//ducrat.changeFollowsConter(true);
				} else {
					me_element.removeClass('is-follow btn-danger').addClass('btn-info');
					me_element.html(me_element.attr('follow-message'));
					//ducrat.changeFollowsConter(false);
				}
			}
		}, function(error){
			b_spin.ladda('stop');
		});
	});

	$('.proc-users-follow').hover(
		function(e){
			following_message = $(this).attr('following-message');
			unfollow_message = $(this).attr('unfollow-message');

			if($(this).hasClass('is-follow')) {
				$(this).html(unfollow_message).addClass('btn-danger').removeClass('btn-info');
			}
		}, 
		function(e) {
			following_message = $(this).attr('following-message');
			unfollow_message = $(this).attr('unfollow-message');

			if($(this).hasClass('is-follow')) {
				$(this).html(following_message).addClass('btn-info').removeClass('btn-danger');
			}	
		}
	);
};
ducrat.profileUploadCover = function(data) {
	$('#user-profile-cover').attr('src', data);

	coreapp.postRequest(
	'POST',
	'me/update/cover',
	{cover: data},
	function(successResponse){
		if(successResponse.error) {
			coreapp.notificate('', 'danger', '', successResponse.message);
		} else {
			coreapp.notificate('', 'success', '', successResponse.message);
		}
	},
	function(errorResponse){
		coreapp.notificate('', 'danger', '', i18n.user.config.error.account.catch);
	});
};
ducrat.profileUploadPhoto = function(data) {
	$('#user-profile-photo').attr('src', data);

	coreapp.postRequest(
	'POST',
	'me/update/photo',
	{photo: data},
	function(successResponse){
		if(successResponse.error) {
			coreapp.notificate('', 'danger', '', successResponse.message);
		} else {
			coreapp.notificate('', 'success', '', successResponse.message);
		}
	},
	function(errorResponse){
		coreapp.notificate('', 'danger', '', i18n.user.config.error.account.catch);
	});
};
//DUCRAT CONFIGURATION
ducrat.ConfigurationAccount = function() {
	var l = $('.ladda-button').ladda();

	$('form').validate({
		rules: {
			first_name: {
				required: true,
				minlength: 3
			},
			last_name: {
				required: true,
				minlength: 3
			},
			username: {
				required: true,
				pattern: /^([a-zA-Z0-9.]{3,16})$/,
				remote: {
					url: coreapp.config.api_url + 'me/verify/username',
					type: 'post'
				}
			},
			email: {
				required: true,
				email: true,
				remote: {
					url: coreapp.config.api_url + 'me/verify/useremail',
					type: 'post'
				}
			}
		},
		messages: {
			first_name: {
				required: i18n.user.config.first_name.required,
				minlength: i18n.user.config.first_name.minlength
			},
			last_name: {
				required: i18n.user.config.first_name.required,
				minlength: i18n.user.config.first_name.minlength
			},
			username: {
				required: i18n.user.config.username.required,
				pattern: i18n.user.config.username.pattern,
				remote: i18n.user.config.username.remote
			},
			email: {
				required: i18n.user.config.email.required,
				email: i18n.user.config.email.mail,
				remote: i18n.user.config.email.remote
			}	
		},
		submitHandler: function (form) {
			l.ladda('start');

			coreapp.postRequest(
			'POST',
			'me/config/account',
			$(form).serialize(),
			function(successResponse){
				if(successResponse.error) {
					coreapp.notificate('', 'danger', '', successResponse.message);
					l.ladda('stop');
				} else {
					coreapp.notificate('', 'success', '', successResponse.message);
					l.ladda('stop');
				}
			},
			function(errorResponse){
				coreapp.notificate('', 'danger', '', i18n.user.config.error.account.catch);
				l.ladda('stop');
			});

			return false;
		}
	});
};
ducrat.ConfigurationProfile = function() {
	var l = $('.ladda-button').ladda();

	$('#id_region').select2()
	$('#id_timezone').select2()

	$('form').validate({
		rules: {
			biografia: {
				required: false,
			},
			region: {
				required: true
			},
			timezone: {
				required: true
			}
		},
		messages: {
			region: {
				required: i18n.user.config.region.required
			},
			timezone: {
				required: i18n.user.config.timezone.required
			}
		},
		submitHandler: function (form) {
			l.ladda('start');

			coreapp.postRequest(
			'POST',
			'me/config/profile',
			$(form).serialize(),
			function(successResponse){
				if(successResponse.error) {
					coreapp.notificate('', 'danger', '', successResponse.message);
					l.ladda('stop');
				} else {
					coreapp.notificate('', 'success', '', successResponse.message);
					l.ladda('stop');
				}
			},
			function(errorResponse){
				coreapp.notificate('', 'danger', '', i18n.user.config.error.profile.catch);
				l.ladda('stop');
			});

			return false;
		}
	});
};
ducrat.startSocialFeed = function() {
	var textarea = $('#social-feed-textarea');
	//autosize(textarea);
	ducrat.config.feed.chanel = JSON.parse($('#social-feed-box').attr('chanel'));
	ducrat.config.feed.chanel.page = 1;

	ducrat.socialFeedUploadPhoto($('#user-upload-photo'));
	ducrat.socialFeedPost();
};
ducrat.articleGetChanel = function() {
	element = $('#articles-result-content');
	ducrat.config.articles.chanel = JSON.parse(element.attr('chanel'));
	
	coreapp.postRequest('GET', 'articles', ducrat.config.articles.chanel,
	function(response){
		if(response.error) {

		} else {
			response_elements = $(response.articles);
			element.html(response_elements);
		}
	}, function(error){
		console.log('error', error);
	});
}
ducrat.articleStartCreator = function() {
	$('#article-textarea').summernote({
		minHeight: 300,
		tooltip: false,
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']]
		]
	});
};
ducrat.articleSetCover = function(data) {
	$('#article-header-content').css('background-image', 'url("'+data+'")');
	$('#id-article-cover').val(data);
};
ducrat.cropperUploadImage = function() {
	$('.crop-image-upload').change(function() {
	    var fileReader = new FileReader(),
	    	filecontent = $(this),
	    	files = this.files,
	    	modal = $('#uploader-images'),
	    	modal_body = $('#uploader-images-body'),
	    	loader = $('<div class="card-loader"><i class="fas fa-spinner fa-spin"></i></div>');

	    if (!files.length) {
	        return;
	    }

	    file = files[0];

	    if (/^image\/\w+$/.test(file.type)) {
	        fileReader.readAsDataURL(file);

	        modal_body.html(loader).addClass('card-load');
	        modal.modal('show');

	        fileReader.onload = function () {
	        	modal_body.html(loader).removeClass('card-load');
	        	modal_body.html('<img src="'+this.result+'" id="uploader-image-image" />')
	        	jq_imagen = $('#uploader-image-image');

	        	ducrat.config.uploader.image = jq_imagen.cropper({
	        		aspectRatio: parseInt(filecontent.attr('w_ratio')) / parseInt(filecontent.attr('h_ratio')),
	        		viewMode: 1
	        	});

	        	ducrat.cropperUploadActions(filecontent.attr('imagefor'), modal);
	        };
	    } else {
	    	toastr.error('Wrong file, select an image.');
	    }
	});
};
ducrat.cropperUploadActions = function(action, modal) {
	ducrat.config.uploader.action = action;
	$('#uploader-images-action').on('click', function(e){
		data = ducrat.config.uploader.image.cropper('getCroppedCanvas').toDataURL()
		modal.modal('toggle');

		console.log(action, ducrat.config.uploader.action);

		switch(action) {
			case 'cover':
				ducrat.profileUploadCover(data);
				break;
			case 'profile':
				ducrat.profileUploadPhoto(data);
				break;
			case 'article':
				ducrat.articleSetCover(data);
				break;
		}
	});
};
$(document).ready(function() {
	if($('.proc-users-follow').length > 0) ducrat.profileUserFriendship();
	if($('#config-form-account').length > 0) ducrat.ConfigurationAccount();
	if($('#config-form-profile').length > 0) ducrat.ConfigurationProfile();
	if($('#social-feed-box').length > 0) ducrat.startSocialFeed();
	if($('#social-feed-post-content').length > 0) ducrat.socialFeedLoadPost();
	if($('#article-form').length > 0) ducrat.articleStartCreator();
	if($('.crop-image-upload').length > 0) ducrat.cropperUploadImage();
	if($('#articles-result-content').length > 0) ducrat.articleGetChanel();
});
