var ducrat = ducrat || {};
ducrat.config = {
	user: {
		more_friends: true,
		friendship_page: 1,
	},
	feed: {
		chanel: {},
		media_photo: 0,
	}
}
ducrat.UserFriendshipActions = function() {
	$('.proc-users-follow').unbind('click');
	$('.proc-users-follow').unbind('hover');

	$('.proc-users-follow').on('click', function(e){
		e.preventDefault();
		var me_element = $(this),
			b_spin = me_element.ladda();

		b_spin.ladda('start');
		coreapp.postRequest('POST', 'me/friendship', {user:me_element.attr('username')},
		function(response){
			b_spin.ladda('stop');
			if(response.error) {
				toastr.error(response.message)
			} else {
				if(response.action) {
					me_element.addClass('is-follow');
					me_element.html(me_element.attr('following-message'));
					ducrat.changeFollowsConter(true);
				} else {
					me_element.removeClass('is-follow');
					me_element.html(me_element.attr('follow-message'));
					ducrat.changeFollowsConter(false);
				}
			}
		}, function(error){
			b_spin.ladda('stop');
		});
	});

	$('.proc-users-follow').hover(function(e){
		console.log('button hover');
	});
}
ducrat.changeFollowsConter = function(when) {
	if($('#user-follows-conter').length > 0) {
		last_val = parseInt($('#user-follows-conter').attr('conter'));
		new_val = ((when) ? last_val+1 : last_val-1);
		console.log(new_val);
		$('#user-follows-conter').attr('conter', new_val).html(new_val);
	}
}
ducrat.UserGetFriendship = function() {
	if(ducrat.config.user.more_friends) {
		var me_element = $('#proc-user-get-friendship');
		coreapp.postRequest('POST', 'me/get-friends', {user:me_element.attr('username'), action:me_element.attr('get'), page:ducrat.config.user.friendship_page},
		function(response){
			if(response.error) {
				toastr.error(response.message)
			} else {
				if(response.friends) {
					me_element.append(response.friends);
					ducrat.UserFriendshipActions();
				} else {
					ducrat.config.user.more_friends = false;
				}
			}
		}, function(error){
			console.log(error);
		});
	}
}
ducrat.socialFeedUploadPhoto = function($inputImage) {
	$inputImage.change(function() {
	    var fileReader = new FileReader(),
	            files = this.files,
	            file,
	            content_image = ducrat.socialFeedNewImage();

	    if(ducrat.config.feed.media_photo == 3) $inputImage.prop('disabled', true);

	    if (!files.length) {
	        return;
	    }

	    file = files[0];

	    if (/^image\/\w+$/.test(file.type)) {
	        fileReader.readAsDataURL(file);
	        fileReader.onload = function () {
	            $inputImage.val("");
	            content_image.children('.image-content').html('<input type="hidden" name="post-gallery[]" value="'+this.result+'" /><img src="'+this.result+'" class="img-responsive" />');
	        };
	    } else {
	    	toastr.error('Wrong file, select an image.');
	    }
	});
}
ducrat.socialFeedNewImage = function() {
	to_add_elements = $('#social-feed-media-content');
	if(ducrat.config.feed.media_photo === 0) to_add_elements.show();

	ducrat.config.feed.media_photo++;
	image_object = $('<div class="feed-thumbnail-imagecontainer col-md-4" id="Image-'+ducrat.config.feed.media_photo+'"><span class="feed-drop-image" cursor="'+ducrat.config.feed.media_photo+'">x</span><div class="image-content"></div></div>');
	to_add_elements.append(image_object);
	ducrat.socialFeedDeleteImage();
	return image_object;
}
ducrat.socialFeedDeleteImage = function() {
	$('.feed-drop-image').unbind('click');
	$('.feed-drop-image').on('click', function(){
		cursor = $(this).attr('cursor');
		image_element = $('#Image-' + cursor);
		image_element.remove();
		if(ducrat.config.feed.media_photo == 3) $('#user-upload-photo').prop('disabled', false);
		ducrat.config.feed.media_photo--;
		if(ducrat.config.feed.media_photo == 0) $('#social-feed-media-content').hide();
	})
}
ducrat.socialFeedLikeAction = function() {
	$('.feed-post-action-like').unbind('click');
	$('.feed-post-action-like').on('click', function(){
		var me_element = $(this);
		coreapp.postRequest('POST', 'me/post/like', {'post':me_element.attr('post')},
		function(response){
			if(response.error) {
				toastr.error(response.message)
			} else {
				if(response.action) {
					me_element.addClass('btn-primary').removeClass('btn-white');
				} else {
					me_element.addClass('btn-white').removeClass('btn-primary');
				}
			}
		}, function(error){
			console.log('error', error);
		});
	});
}
ducrat.socialFeedCommentShow = function() {
	$('.feed-post-action-comment').unbind('click')
	$('.feed-post-action-comment').on('click', function(){
		var me_element = $(this),
			comment_element = $('#feed-post-action-comment-' + me_element.attr('post')),
			comment_textarea = $('#feed-post-textarea-comment-' + me_element.attr('post'));

		autosize(comment_textarea);
		ducrat.socialFeedCommentAction(comment_textarea);
		comment_element.removeClass('hide');
	});
}
ducrat.socialFeedCommentAction = function(textarea) {
	textarea.focus();
	textarea.unbind('keypress');
	textarea.on('keypress', function(e){
		var code = (e.keyCode ? e.keyCode : e.which);
		if (code == 13) {
			if(textarea.val()) {
				coreapp.postRequest('POST', 'me/post/comment', {'post':textarea.attr('post'), 'comment':textarea.val()},
				function(response){
					if(response.error) {
						toastr.error(response.message);
					} else {
						$('.feed-subcoment-content').append(response.comment);
						textarea.val('');
						return false;
					}
				}, function(error){
					console.log(error);
				});
			} else {
				return false;
			}
		}
	});
}
ducrat.socialFeedSharePost = function() {
	$('.feed-post-action-share').unbind('click');
	$('.feed-post-action-share').on('click', function(){
		var me_element = $(this);
		coreapp.postRequest('GET', 'me/post/share', {'post':me_element.attr('post')},
		function(response){
			if(response.error) {
				toastr.erro(response.message);
			} else {
				comment_textarea = $('#feed-post-textarea-share');
				autosize(comment_textarea);
				$('#feed-share-post-subpost').html(response.comment);
				$('#social-feed-share').modal('show');
				ducrat.socialFeedSharePostAction(me_element.attr('post'), comment_textarea);
			}
		}, function(error){
			console.log(error);
		});
	});
}
ducrat.socialFeedSharePostAction = function(post, text){
	$('#social-feed-share-post').unbind('click');
	$('#social-feed-share-post').on('click', function(){
		var b_spin = $(this).ladda();
		b_spin.ladda('start');
		try {
			coreapp.postRequest('POST', 'me/post/share', {'comment':text.val(), 'post':post},
			function(response){
				b_spin.ladda('stop');
				if(response.error) {
					toastr.error(response.message);
				} else {
					$('#social-feed-post-content').prepend(response.comment);
					$('#feed-share-post-subpost').html('');
					$('#social-feed-share').modal('hide');
					ducrat.socialFeedPostActions();
				}
			}, function(error){
				b_spin.ladda('stop');
				console.log(error);
			});
		} catch(error) {
			console.log(error);
			b_spin.ladda('stop');
		}
	});
}
ducrat.socialFeedPostActions = function() {
	ducrat.socialFeedLikeAction();
	ducrat.socialFeedCommentShow();
	ducrat.socialFeedSharePost();
}
ducrat.socialFeedLoadPost = function() {
	coreapp.postRequest('GET', 'post', ducrat.config.feed.chanel,
	function(response){
		if(response.error) {

		} else {
			html_elements = $(response.comment);
			$('#social-feed-post-content').append(html_elements);
			console.log(html_elements);
			ducrat.socialFeedPostActions();
		}
	}, function(error){
		console.log('error', error);
	});
}
ducrat.socialFeedPost = function(){
	$('#social-feed-post').on('click', function(){
		var b_spin = $(this).ladda(),
			b_content = $('#social-feed-body-content');

		b_spin.ladda('start');
		b_content.toggleClass('sk-loading');
		try {
			post_data = $('#social-feed-form').serialize();
			post_data += '&' + coreapp.serializeJsonData(ducrat.config.feed.chanel);
			coreapp.postRequest('POST', 'post', post_data,
			function(response){
				$('#social-feed-textarea').val('');
				$('#social-feed-media-content').html('').hide();
				ducrat.config.feed.media_photo = 0;
				b_spin.ladda('stop');
				b_content.toggleClass('sk-loading');

				if(response.error) {
					toastr.error(response.message);
				} else {
					$('#social-feed-post-content').prepend(response.comment);
					ducrat.socialFeedPostActions();
				}
			}, function(error){
				console.log(error);
				b_spin.ladda('stop');
				b_content.toggleClass('sk-loading');
			});
		} catch(error) {
			console.log(error);
			b_spin.ladda('stop');
			b_content.toggleClass('sk-loading');
		}
	});
}
ducrat.startSocialFeed = function() {
	var textarea = $('#social-feed-textarea');
	autosize(textarea);
	ducrat.config.feed.chanel = JSON.parse($('#social-feed-content').attr('chanel'));
	ducrat.config.feed.chanel.page = 1;

	ducrat.socialFeedUploadPhoto($('#user-upload-photo'));
	ducrat.socialFeedLoadPost();
	ducrat.socialFeedPost();
}
$(document).ready(function() {
	if($('.proc-users-follow').length > 0) ducrat.UserFriendshipActions();
	if($('#proc-user-get-friendship').length > 0) ducrat.UserGetFriendship();
	if($('#social-feed-box').length > 0) ducrat.startSocialFeed();
});
