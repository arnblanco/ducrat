i18n = {
	user: {
		config: {
			error:{
				account: {
					catch: 'No se logro enviar tu informacion, intenta mas tarde.'
				},
				profile: {
					catch: 'No se logro enviar tu informacion, intenta mas tarde'
				}
			},
			first_name: {
				required: 'Este campo es necesario.',
				minlength: 'Su nombre es demasiado corto.',
			},
			last_name: {
				required: 'Este campo es necesario.',
				minlength: 'Su apellido es demasiado corto.',
			},
			username: {
				required: 'Ingresa un nombre de usuario.',
				pattern: 'Nombre de usuario invalido.',
				remote: 'Este nombre de usuario no esta disponible'
			},
			email: {
				required: 'Ingresa un correo electrónico.',
				email: 'Tienes que ingresar un correo electrónico.',
				remote: 'Este correo electrónico ya esta siendo usado por otro usuario.'
			},
			region: {
				required: ' : Selecciona una region para mostrarte un mejor contenido.'
			},
			timezone: {
				required: ' : Selecciona una zona horaria.'
			}
		}
	}
}