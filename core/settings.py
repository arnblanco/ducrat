import raven
import os
from os import environ
from os.path import abspath, dirname, join
from datetime import datetime, timedelta
from sys import argv
from configurations import Configuration
from django.utils.translation import ugettext_lazy as _

#Directory paths
BASE_DIR = dirname(dirname(abspath(__file__)))
PROJECT_ROOT = abspath(dirname(__file__))
PROJECT_NAME = 'ducrat'


"""
    Configuracion del servidor de cache de la aplicacion
"""
class RedisCache(object):
    CACHES = {
        'default': {
            'BACKEND': 'django_elasticache.memcached.ElastiCache',
            'LOCATION': 'cubeprogramcache.wxnjie.cfg.usw2.cache.amazonaws.com:11211',
            'OPTIONS': {
                'IGNORE_CLUSTER_ERRORS': [True,False],
            },
        }
    }


"""
    Configuracion las llaves de acceso para los servicios que utiliza esta aplicacion
"""
class KeysData(object):
    # #############################################################################
    # AWS KEYS CONFIG
    # #############################################################################
    AWS_ACCESS_KEY_ID = 'AWS_ACCESS_KEY_ID' in os.environ and os.environ['AWS_ACCESS_KEY_ID'] or ''
    AWS_SECRET_ACCESS_KEY = 'AWS_SECRET_KEY' in os.environ and os.environ['AWS_SECRET_KEY'] or ''

    # #############################################################################
    # CAPCHA KEYS CONFIG
    # #############################################################################
    RECAPTCHA_PUBLIC_KEY = 'RECAPTCHA_PUBLIC_KEY' in os.environ and os.environ['RECAPTCHA_PUBLIC_KEY'] or ''
    RECAPTCHA_PRIVATE_KEY = 'RECAPTCHA_PRIVATE_KEY' in os.environ and os.environ['RECAPTCHA_PRIVATE_KEY'] or ''
    NOCAPTCHA = True

    # #############################################################################
    # FACEBOOK ACCESS CREDENTIALS
    # #############################################################################
    SOCIAL_AUTH_FACEBOOK_KEY = 'SOCIAL_AUTH_FACEBOOK_KEY' in os.environ and os.environ['SOCIAL_AUTH_FACEBOOK_KEY'] or ''
    SOCIAL_AUTH_FACEBOOK_SECRET = 'SOCIAL_AUTH_FACEBOOK_SECRET' in os.environ and os.environ['SOCIAL_AUTH_FACEBOOK_SECRET'] or ''
    SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']

    # #############################################################################
    # TWITTER ACCESS CREDENTIALS
    # #############################################################################
    SOCIAL_AUTH_TWITTER_KEY = 'SOCIAL_AUTH_TWITTER_KEY' in os.environ and os.environ['SOCIAL_AUTH_TWITTER_KEY'] or ''
    SOCIAL_AUTH_TWITTER_SECRET = 'SOCIAL_AUTH_TWITTER_SECRET' in os.environ and os.environ['SOCIAL_AUTH_TWITTER_SECRET'] or ''

    # #############################################################################
    # GOOGLE+ ACCESS CREDENTIALS
    # #############################################################################
    SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = 'SOCIAL_AUTH_GOOGLE_OAUTH2_KEY' in os.environ and os.environ['SOCIAL_AUTH_GOOGLE_OAUTH2_KEY'] or ''
    SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET' in os.environ and os.environ['SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET'] or ''


"""
    Configuracion de Medios Sociales para la Autenticacion de Usuarios
"""
class SocialConfig(object):
    # PYTHON SOCIAL AUTH CONFIGURATION
    SOCIAL_AUTH_URL_NAMESPACE = 'social'

    #Social Auth URL config
    LOGIN_REDIRECT_URL = '/'
    LOGIN_URL = '/'

    SOCIAL_AUTH_LOGIN_URL = '/'
    SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'

    SOCIAL_AUTH_LOGIN_ERROR_URL = '/'
    SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/'

    SOCIAL_AUTH_NEW_ASSOCIATION_REDIRECT_URL = '/'

    SOCIAL_AUTH_PIPELINE = (
        'social_core.pipeline.social_auth.social_details',
        'social_core.pipeline.social_auth.social_uid',
        'social_core.pipeline.social_auth.social_user',
        'social_core.pipeline.social_auth.associate_by_email',
        'social_core.pipeline.user.get_username',
        'social_core.pipeline.user.create_user',
        '_components.social_pipeline.new_users_handler',
        'social_core.pipeline.social_auth.associate_user',
        'social_core.pipeline.social_auth.load_extra_data',
        'social_core.pipeline.user.user_details',
    )


#Global Settings
class Common(Configuration, KeysData, SocialConfig):
    # SECURITY WARNING: keep the secret key used in production secret!
    SECRET_KEY = 'SECRET_KEY' in os.environ and os.environ['SECRET_KEY'] or ''

    #No direccionar al encontrarse sin un slash
    APPEND_SLASH = False
    CORS_ORIGIN_ALLOW_ALL = True

    # SYSTEM ADMINISTRATOR
    ADMINS = (
        ('Arnoldo Blanco', 'oblancomorales@gmail.com'),
    )
    MANAGERS = ADMINS

    # SECURITY WARNING: don't run with debug turned on in production!
    DEBUG = False

    # HOST PERMITIDOS, only for production
    ALLOWED_HOSTS = ['localhost']

    #Aplications for this projects
    INSTALLED_APPS = [
        #Django Aplications
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.humanize',

        #Third parties applications
        'rest_framework',
        'raven.contrib.django.raven_compat',
        'storages',
        'social_django',
        'bootstrap4',
        'corsheaders',

        #Our Applications 
        '_apps.dashboard',
        '_apps.website',
        '_apps.account',
        '_apps.courses',
        '_apps.articles',
        '_apps.comments',
        '_apps.messenger',
        '_apps.system',
    ]

    #Aplications Middlewares
    MIDDLEWARE = [
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'corsheaders.middleware.CorsMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.locale.LocaleMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ]

    #Authentications Backends
    AUTHENTICATION_BACKENDS = [
        'social_core.backends.facebook.FacebookOAuth2',
        'social_core.backends.twitter.TwitterOAuth',
        'social_core.backends.google.GoogleOpenId',
        'social_core.backends.google.GoogleOAuth2',
        'django.contrib.auth.backends.RemoteUserBackend',
    ]

    #URL's Root
    ROOT_URLCONF = 'core.urls'

    #Template Directory and contextprocesors
    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [
                join(BASE_DIR, '_templates')
            ],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                    'social_django.context_processors.backends',
                    'social_django.context_processors.login_redirect',
                ],
            },
        },
    ]

    #RestFramework Authentication
    REST_FRAMEWORK = {
        'DEFAULT_PERMISSION_CLASSES': (
            'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
        ),
        'DEFAULT_AUTHENTICATION_CLASSES': (
            'rest_framework.authentication.BasicAuthentication',
            'rest_framework.authentication.SessionAuthentication',
        )
    }

    #WSGI Application
    WSGI_APPLICATION = 'core.wsgi.application'

    # Database
    # https://docs.djangoproject.com/en/{{ docs_version }}/ref/settings/#databases
    # http://django-configurations.readthedocs.org/en/latest/values/#configurations.values.DatabaseURLValue
    DATABASES = {}

    #Auth Login options
    LOGIN_REDIRECT_URL = '/'
    LOGIN_URL = '/account/login/'

    # Password validation
    # https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators
    AUTH_PASSWORD_VALIDATORS = [
        {
            'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
        },
    ]

    # INTERNATIONALIZATION
    # https://docs.djangoproject.com/en/{{ docs_version }}/topics/i18n/
    USE_I18N = True
    USE_L10N = False
    USE_TZ = False

    #Language Settings
    LANGUAGE_CODE = 'es'
    LANGUAGES = [
        ('es', _('Español')),
        ('de', _('Alemán')),
        ('en', _('Ingles')),
        ('pt', _('Portugués')),
        ('fr', _('Francés'))
    ]

    LOCALE_PATHS = (
        os.path.join(BASE_DIR, '_locales'),
    )

    # Static files (CSS, JavaScript, Images)
    # https://docs.djangoproject.com/en/{{ docs_version }}/howto/static-files/
    STATIC_URL = '/static/'
    STATIC_ROOT = join(PROJECT_ROOT, '_statics')


    # #############################################################################
    # AMAZON SES 
    # #############################################################################
    AWS_SES_REGION_NAME = 'us-west-2'
    AWS_SES_REGION_ENDPOINT = 'email.us-west-2.amazonaws.com'
    EMAIL_BACKEND = 'django_ses.SESBackend'
    DEFAULT_FROM_EMAIL = 'noreply@ducrat.com'

    # #############################################################################
    # AWS S3 STORAGE
    # #############################################################################
    AWS_STORAGE_BUCKET_NAME = '%s-media' % PROJECT_NAME
    BOTO_S3_BUCKET = AWS_STORAGE_BUCKET_NAME
    AWS_S3_SECURE_URLS = True
    AWS_QUERYSTRING_AUTH = False

    DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

    future = datetime.now() + timedelta(days=10)
    AWS_HEADERS = {
        'Expires': future.strftime('%a, %d %b %Y %H:%M:%S GMT'),
        'Cache-Control': 'max-age=86400, public'
    }


    # #############################################################################
    # MEDIA & STATIC & TEMPLATES
    # #############################################################################
    MEDIA_URL = "http://s3-%s.amazonaws.com/%s/" % (AWS_SES_REGION_NAME, AWS_STORAGE_BUCKET_NAME)

    # Static compilers
    STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    )

    # Additional locations of static files
    STATICFILES_DIRS = [
        join(BASE_DIR, '_statics'),
        join(BASE_DIR, '_nodemodules'),
    ]

    FIXTURE_DIRS = [
        join(BASE_DIR, '_fixtures')
    ]

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': True,
        'root': {
            'level': 'WARNING',
            'handlers': ['sentry'],
        },
        'formatters': {
            'verbose': {
                'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
            },
        },
        'handlers': {
            'sentry': {
                'level': 'ERROR',
                'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            },
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'verbose'
            }
        },
        'loggers': {
            'django.db.backends': {
                'level': 'ERROR',
                'handlers': ['console'],
                'propagate': False,
            },
            'raven': {
                'level': 'DEBUG',
                'handlers': ['console'],
                'propagate': False,
            },
            'sentry.errors': {
                'level': 'DEBUG',
                'handlers': ['console'],
                'propagate': False,
            },
        },
    }


# DEVELOPERS settings extended to Common
class Developers(Common):
    #Debugs Options
    DEBUG = True
    ALLOWED_HOSTS = ['localhost', '127.0.0.1']

    # Database
    # https://docs.djangoproject.com/en/{{ docs_version }}/ref/settings/#databases
    # http://django-configurations.readthedocs.org/en/latest/values/#configurations.values.DatabaseURLValue
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'ducrat',
            'USER': 'ducrat',
            'PASSWORD': 'du123$',
            'HOST': 'localhost',
            'PORT': 5432,
        }
    }

    #Extra Config
    AWS_ACCESS_KEY_ID = 'AKIAJYIU4YO7NSDHTNHQ'
    AWS_SECRET_ACCESS_KEY = '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'

    RECAPTCHA_PUBLIC_KEY = '6LevJx4UAAAAAF_UAu1298dZU1shKZ-i09RDZnWr'
    RECAPTCHA_PRIVATE_KEY = '6LevJx4UAAAAAPpavdrvoQ-KmJT4N6AQ7IZVgAPk'

    SOCIAL_AUTH_FACEBOOK_KEY = '2012656138745430'
    SOCIAL_AUTH_FACEBOOK_SECRET = 'dbf26cfca54552026c257208f891f60b'
    SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']

    SOCIAL_AUTH_TWITTER_KEY = '4RRChAMEpJLvTsNO21kjFFLvY'
    SOCIAL_AUTH_TWITTER_SECRET = 'IvUdIUhG7Rjo14JwXpWczWr0kAVvOBqJbjOHMgvtMQMwU1WfG7'

    SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '712731065070-a3rfqu1a9tielefeahujjlraq59ekatt.apps.googleusercontent.com'
    SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'mQ9KdLnISo02VPMdX1zeRL05'

    SECRET_KEY = 'x6a^^jupw$0k=3_1jgi9llj72e%pb2g149)anb#x1n@e%#y!rbpy'

    #Mail BACKEND Config
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = join(BASE_DIR, 'app-emails')



# DEVELOPERS settings extended to Common
class Production(Common):
    RAVEN_CONFIG = {
        'dsn': 'https://52f882b729724913a9178a40fed65845:58a89eeb8b6442acb4b48a4c9983fbf0@sentry.io/1225657',
    }