from django.urls import include, path
from django.contrib import admin

urlpatterns = [
    #------------------USER GENERAL ACTIONS---------------------------------------------------------#
    path('', include('_apps.dashboard.urls', namespace='urls_dash')),
    path('u/', include('_apps.account.users_urls', namespace='urls_u')),
    path('c/', include('_apps.courses.courses_urls', namespace='urls_c')),

    path('account/', include('_apps.account.urls', namespace='urls_account')),
    path('settings/', include('_apps.account.settings_urls', namespace='urls_settings')),
    path('comments/', include('_apps.comments.urls', namespace='urls_comments')),
    path('courses/', include('_apps.courses.urls', namespace='urls_courses')),
    path('post/', include('_apps.articles.urls', namespace='urls_articles')),

    #path('messages/', include('_apps.messenger.urls', namespace='urls_messages')),
    path('site/', include('_apps.website.urls', namespace='urls_website')),
    path('social/', include('social_django.urls', namespace='social')),

    #------------------PROFESSIONAL ACTIONS-----------------------------------------------------------#
    path('teach/', include('_apps.teach.urls', namespace='urls_teach')),

    #------------------REST API-----------------------------------------------------------------------#
    path('api/', include('_apis.urls', namespace='api_urls')),
]
