# -*- coding: utf-8 -*-
import base64
import hashlib
import json
import pytz

from django.conf import settings
from django.core.files.base import ContentFile
from django.db.models import Q
from django.http import HttpResponse
from django.utils import timezone
from django.utils.translation import gettext as _
from django.template import loader
from django.views.decorators.csrf import csrf_exempt

from random import choice

from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.parsers import JSONParser
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer

from django.contrib.auth.models import User
from _apps.comments.models import Comment
from _apps.articles.models import Articulo


#Plantillas para los comentarios
comment_one_layout = ['single']
comment_two_layout = ['ly-two-hr', 'ly-two-vr']
comment_three_layout = ['ly-three-hr-bss', 'ly-three-hr-ssb', 'ly-three-vr-bss', 'ly-three-vr-ssb']


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


"""
--------------------- CONFIGURACION GENERAL DE LA CUENTA ---------------------------
"""
@api_view(['GET', 'POST'])
@permission_classes((IsAuthenticated,))
@csrf_exempt
def post_comment(request, one_comment='comments/one_comment.html', more_comment='comments/many_comments.html'):
	data_response = []
	user = request.user

	if request.GET:
		l_comments = get_post_by_chanel(request)
		comments = []

		for com in l_comments:
			com.is_like = com.verify_like(user)

			if not com.share:
				all_images = com.commentimages_set.all()
				com.images_all = all_images
				com.images_total = len(all_images)
				com.images_layout = get_image_layout(all_images)
			else:
				subcom = com.share
				all_images = subcom.commentimages_set.all()
				subcom.images_all = all_images
				subcom.images_total = len(all_images)
				subcom.images_layout = get_image_layout(all_images)

				com.share = subcom

			comments.append(com)

		template = loader.get_template(more_comment)
		content = template.render({'comments':comments, 'chanel':request.GET['chanel']})
		data_response = {'error':False, 'comment':content}
	elif request.POST:
		tag_user, article = None, None

		try:
			comment_text = request.POST['post-comment']
		except:
			comment_text = ''

		try:
			chanel = request.POST['chanel']
		except:
			chanel = ''

		if chanel == 'tag-serach':
			try:
				tag = request.POST['user']
			except:
				tag = ''

			if not '#%s' % tag in comment_text:
				comment_text = '%s #%s' % (comment_text, tag) if comment_text else '#%s' % tag
		else:
			try:
				user_name = request.POST['user']
				if(user_name and not user.username == user_name):
					tag_user = User.objects.get(username=user_name)
			except:
				tag_user = None


			try:
				artid = request.POST['user']
				if(artid and not artid==''):
					article = Articulo.objects.get(id=artid)
			except:
				article = None

		comment = user.comment_set.create(comment=comment_text, taged_user=tag_user, article=article)

		if 'post-gallery[]' in request.POST:
			photos_load = request.POST.getlist('post-gallery[]')
			for photo in photos_load:
				exdata, imgstr = photo.split(';base64,')
				ext = exdata.split('/')[-1]

				imagen = comment.commentimages_set.create()

				image_name = '%s-%s-%s.%s' % (user.id, comment.id, imagen.id, ext)
				imagen.image.save(image_name, ContentFile(base64.b64decode(imgstr)))
				imagen.save()

		all_images = comment.commentimages_set.all()
		comment.images_all = all_images
		comment.images_total = len(all_images)
		comment.images_layout = get_image_layout(all_images)

		template = loader.get_template(one_comment)
		content = template.render({'comment':comment, 'user':user})
		data_response = {'error':False, 'comment':content}

	return JSONResponse(data_response, status=202)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@csrf_exempt
def post_like(request):
	data_response = []
	user = request.user

	try:
		comment = Comment.objects.get(id=request.POST['post'])
	except:
		comment = None


	if comment:
		try:
			like = user.likes_set.get(comment=comment)
		except:
			like = None

		if like:
			like.delete()
			data_response = {'error': False, 'action': False}
		else:
			like = user.likes_set.create(comment=comment)
			data_response = {'error': False, 'action': True}
	else:
		data_response = {'error': True, 'message': _('No se localizo el comentario seleccionado, verifique e intente nuevamente.')}


	return JSONResponse(data_response, status=202)


@api_view(['GET', 'POST'])
@permission_classes((IsAuthenticated,))
@csrf_exempt
def post_share(request, default_template='comments/share_comment.html', one_comment='comments/one_comment.html'):
	data_response = []
	user = request.user

	if request.GET:
		try:
			comment = Comment.objects.get(id=request.GET['post'])
		except:
			comment = None


		if comment:
			comment = comment if not comment.share else comment.share

			all_images = comment.commentimages_set.all()
			comment.images_all = all_images
			comment.images_total = len(all_images)
			comment.images_layout = get_image_layout(all_images)

			template = loader.get_template(default_template)
			content = template.render({'comment':comment})
			data_response = {'error':False, 'comment':content}
		else:
			data_response = {'error': True, 'message': _('No se localizo el comentario seleccionado, verifique e intente nuevamente.')}
	if request.POST:
		try:
			comment = Comment.objects.get(id=request.POST['post'])
			comment = comment if not comment.share else comment.share
		except:
			comment = None


		share = user.comment_set.create(
			comment = request.POST['comment'],
			share=comment
		)

		template = loader.get_template(one_comment)
		content = template.render({'comment':share, 'user':user})
		data_response = {'error':False, 'comment':content}

	return JSONResponse(data_response, status=202)



####################################################################################################
##FUNCIONES ADICIONALES
####################################################################################################
def get_post_by_chanel(request):
	user = request.user

	try:
		chanel = request.GET['chanel']
	except:
		chanel = ''

	try:
		page = int(request.GET['page']) if 'page' in request.GET else 1
	except:
		page = 1

	end = page * 8
	start = end - 8

	if chanel:
		verify_chanels = ('profile-me', 'profile-you', 'articles', 'tag-serach')

		if chanel in verify_chanels:
			if chanel == 'profile-me':
				follows = user.follower.all().values('follow')
				return Comment.objects.filter(Q(user=user)|Q(user__in=follows)|Q(taged_user=user))[start:end]
			elif chanel == 'profile-you':
				try:
					puser = User.objects.get(username=request.GET['user'])
				except User.DoesNotExist:
					puser = None

				if puser:
					return Comment.objects.filter(Q(user=puser)|Q(taged_user=puser))[start:end]
				else:
					return []
			elif chanel == 'articles':
				try:
					art = Articulo.objects.get(id=request.GET['user'])
				except Articulo.DoesNotExist:
					art = None

				if art:
					return Comment.objects.filter(article=art)[start:end]

			elif chanel == 'tag-serach':
				tag = '#%s' % request.GET['user']
				return Comment.objects.filter(comment__icontains=tag)[start:end]
		else:
			return []
	else:
		return []



def get_image_layout(all_images):
	if len(all_images) == 3:
		images_layout = choice(comment_three_layout)
	elif len(all_images) == 2:
		images_layout = choice(comment_two_layout)
	else:
		images_layout = 'single'

	return images_layout