from django.urls import path

from _apis.v1 import users, comments, articles

app_name = 'resapi-koala'

urlpatterns = [
	#ADMINISTRIACION DE POST
	path('post', comments.post_comment, name='api_post_comment'),
	path('post/like', comments.post_like, name='api_post_like'),
	path('post/share', comments.post_share, name='api_post_share'),

	#ADMINISTRACION DE ARTICULOS
	path('articles', articles.articles, name='api_articles'),
	
	#CONFIGURAR CUENTA DE USUARIO
	path('me/action/friendship', users.me_friendship, name='api_users_action_friendship'),

	#ACCIONES DEL USUARIO SOBRE SU CUENTA
	path('me/update/photo', users.update_user_photo, name='api_users_update_photo'),
	path('me/update/cover', users.update_user_cover, name='api_users_update_cover'),

	#CONFIGURAR CUENTA DE USUARIO
	path('me/config/account', users.configuracion_cuenta, name='api_users_config_account'),
	path('me/config/profile', users.configuracion_profile, name='api_users_config_profile'),
	
	#VERIFICACION DE DATA GENERAL SIENDO USUARIO
	path('me/verify/username', users.verify_username, name='api_users_verify_username'),
	path('me/verify/useremail', users.verify_useremail, name='api_users_verify_useremail'),
]