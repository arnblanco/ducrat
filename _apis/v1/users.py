# -*- coding: utf-8 -*-
import base64
import json
import pytz


from django.conf import settings
from django.core.files.base import ContentFile
from django.db.models import Q
from django.http import HttpResponse
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_exempt

from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.parsers import JSONParser
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer

from django.contrib.auth.models import User

from _apps.system.models import Regions


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


"""
--------------------- ACCIONES DEL USUARIO ---------------------------
"""
@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@csrf_exempt
def me_friendship(request):
	data_response = []
	user = request.user

	try:
		follow_to = User.objects.get(username=request.POST['user'])
	except:
		follow_to = None


	if follow_to:
		try:
			fw = user.follower.get(follow=follow_to)
		except:
			fw = None

		if fw:
			fw.delete()
			data_response = {'error': False, 'action': False}
		else:
			fw = user.follower.create(follow=follow_to)
			data_response = {'error': False, 'action': True}
	else:
		data_response = {'error': True, 'message': _('No se localizo al usuario solicitado, intente nuevamente.')}


	return JSONResponse(data_response, status=202)


"""
--------------------- UPDATE USER INFORMATION ---------------------------
"""
#Cambiar el cover del usuario
@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@csrf_exempt
def update_user_photo(request):
	user = request.user
	profile = user.profile

	try:
		photo = request.POST['photo']
		exdata, imgstr = photo.split(';base64,')
		ext = exdata.split('/')[-1]

		if profile.picture:
			profile.picture.delete(save=False)

		image_name = '%s-photo.%s' % (user.id, ext)
		profile.picture.save(image_name, ContentFile(base64.b64decode(imgstr)))
		profile.save()

		return JSONResponse({'error': False, 'message':_('Se actualizo tu fotografia de perfil.')}, status=202)
	except:
		return JSONResponse({'error': True, 'message':_('No se logro cargar la fotografia.')}, status=202)


#Cambiar el cover del usuario
@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@csrf_exempt
def update_user_cover(request):
	user = request.user
	profile = user.profile

	try:
		photo = request.POST['cover']
		exdata, imgstr = photo.split(';base64,')
		ext = exdata.split('/')[-1]

		if profile.cover:
			profile.cover.delete(save=False)

		image_name = '%s-cover.%s' % (user.id, ext)
		profile.cover.save(image_name, ContentFile(base64.b64decode(imgstr)))
		profile.save()

		return JSONResponse({'error': False, 'message':_('Se actualizo tu fotografia de portada.')}, status=202)
	except:
		return JSONResponse({'error': True, 'message':_('No se logro cargar la fotografia.')}, status=202)


"""
--------------------- CONFIGURACION GENERAL DE LA CUENTA ---------------------------
"""
#Configurar informacion general de la cuenta
@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@csrf_exempt
def configuracion_cuenta(request):
	user = request.user
	profile = user.profile

	user_mail_change = False
	data_response = {'error':False}

	#Verificar Informacion del nombre de usuario y Correo Electrónico
	try:
		username = request.POST['username']
	except:
		username = ''

	try:
		useremail = request.POST['email']
	except:
		useremail = ''

	if(username == '' or useremail == ''):
		return JSONResponse({'error': True, 'message':_('Ingrese un nombre de usuario y un correo electrónico.')}, status=202)

	if username != user.username:
		otheruser = User.objects.filter(username=username)

		if otheruser:
			return JSONResponse({'error': True, 'message':_('El nombre de usuario no esta disponible.')}, status=202)
		else:
			user.username = username

	if useremail != user.email:
		otheruser = User.objects.filter(email=useremail)

		if otheruser:
			return JSONResponse({'error': True, 'message':_('Correo Electrónico esta siendo utilizado por otra cuenta.')}, status=202)
		else:
			user.email = useremail
			user_mail_change = True

	#Verificacion del cambio de correo electronico
	if user_mail_change:
		profile.mail_verified = False
		profile.save()

		profile.verify_my_email()

	#Guardar datos generales del usuario
	user.save()
	return JSONResponse({'error': False, 'message':_('Información actualizada.')}, status=202)


#Configurar informacion del perfil
@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@csrf_exempt
def configuracion_profile(request):
	user = request.user
	profile = user.profile

	profile.biografia = request.POST['biografia'] if 'biografia' in request.POST else ''
	profile.gender = request.POST['gender'] if('gender' in request.POST and request.POST['gender'] in ('i', 'f', 'm')) else 'i'
	profile.lenguaje = request.POST['lenguaje'] if any(request.POST['lenguaje'] in lng for lng in settings.LANGUAGES) else 'en'
	profile.timezone = request.POST['timezone'] if any(request.POST['timezone'] in tmz for tmz in pytz.common_timezones) else 'America/New_York'

	try:
		reg = Regions.objects.get(id=request.POST['region'])
	except:
		reg = None

	profile.region = reg
	profile.save()

	return JSONResponse({'error': False, 'message':_('Información actualizada correctamente.')}, status=202)


"""
----------------------------VERIFICACION GENERAL DE INFORMACION-------------------------
"""
#Verificar nombres de usaurios
@api_view(['POST'])
@authentication_classes((SessionAuthentication, BasicAuthentication))
@permission_classes((IsAuthenticated,))
@csrf_exempt
def verify_username(request):
	user = request.user
	errors_on_verify = False
	data_response = "true"

	#Verificar Informacion del nombre de usuario y Correo Electrónico
	try:
		username = request.POST['username']
	except:
		username = ''

	if username == '':
		errors_on_verify = False
		data_response = {_('Ingrese un nombre de usuario.')}


	if(not errors_on_verify and username != user.username):
		otheruser = User.objects.filter(username=username)

		if otheruser:
			errors_on_verify = True
			data_response = {_('El nombre de usuario ya esta siendo utilizado')}

	if errors_on_verify:
		return JSONResponse(data_response, status=202)
	else:
		return HttpResponse(data_response, status=202)	


#Verificar correo electronico del usuario
@api_view(['POST'])
@authentication_classes((SessionAuthentication, BasicAuthentication))
@permission_classes((IsAuthenticated,))
@csrf_exempt
def verify_useremail(request):
	user = request.user
	errors_on_verify = False
	data_response = "true"

	#Verificar Informacion del nombre de usuario y Correo Electrónico
	try:
		useremail = request.POST['email']
	except:
		useremail = ''

	if useremail == '':
		errors_on_verify = True
		data_response = {_('Ingrese un correo electrónico.')}


	if(not errors_on_verify and useremail != user.email):
		otheruser = User.objects.filter(email=useremail)

		if otheruser:
			errors_on_verify = True
			data_response = {_('El correo electrónico esta siendo utilizado por otra cuenta.')}

	if errors_on_verify:
		return JSONResponse(data_response, status=202)
	else:
		return HttpResponse(data_response, status=202)
