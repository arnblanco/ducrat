# -*- coding: utf-8 -*-
import base64
import hashlib
import json
import pytz

from django.conf import settings
from django.core.files.base import ContentFile
from django.db.models import Q
from django.http import HttpResponse
from django.utils import timezone
from django.utils.translation import gettext as _
from django.template import loader
from django.views.decorators.csrf import csrf_exempt

from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.parsers import JSONParser
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer

from django.contrib.auth.models import User
from _apps.articles.models import Articulo


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


"""
--------------------- CONFIGURACION GENERAL DE LA CUENTA ---------------------------
"""
@api_view(['GET'])
@permission_classes((IsAuthenticated,))
@csrf_exempt
def articles(request, single_template='articles/many_articles.html'):
	data_response = []
	user = request.user

	articles, chanel = get_articles_by_chanel(request)

	template = loader.get_template(single_template)
	content = template.render({'articles':articles, 'chanel':chanel})
	data_response = {'error':False, 'articles':content}

	return JSONResponse(data_response, status=202)


def get_articles_by_chanel(request):
	user = request.user
	articles = []

	try:
		chanel = request.GET['chanel']
	except:
		chanel = ''

	if chanel == 'single':
		articles = Articulo.objects.filter()[:8]
	elif chanel == 'me-articles':
		articles = user.articulo_set.filter()[:8]
	elif chanel == 'you-articles':
		try:
			puser = User.objects.get(username=request.GET['user'])
		except User.DoesNotExist:
			puser = None

		if puser:
			articles = puser.articulo_set.filter()[:8]

	return articles, chanel
