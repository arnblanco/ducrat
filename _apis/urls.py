from django.urls import include, path

app_name = 'resapi-koala'

urlpatterns = [
	path('koala/v1/', include('_apis.v1.urls', namespace='koala_v1_urls')),
]