# -*- coding: utf-8 -*-
import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, JsonResponse
from django.utils.translation import ugettext as _
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_protect, csrf_exempt

from .models import Comment


#--------------------------------------------OPCIONES DEL PERFIL DEL USUARIO----------------------------------#
#Pefil del comercio
@csrf_protect
@login_required()
def comment_detail(request, cid, response_template='comments/comment_detail.html', blocked_content='comments/block_content.html'):
	user = request.user

	
	context = {'per':per}
	return render(request, response_template, context)



#Pefil del comercio
@csrf_protect
@login_required()
def tag_serach(request, response_template='comments/tag_search.html'):
	user = request.user
	tag = request.GET['tag'] if 'tag' in request.GET else ''

	context = {'tag':tag}
	return render(request, response_template, context)