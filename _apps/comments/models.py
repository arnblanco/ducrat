# -*- coding: utf-8 -*-
import os
import re

from time import gmtime, strftime
from urllib.parse import urlencode

from django.contrib.auth.models import User
from django.db import models

from _apps.articles.models import Articulo


def upload_to_static_file(instance, filename):
	name, ext = os.path.splitext(filename)
	file_key = '%s-%s-%s' % (instance.comment.user.id, instance.id, instance.comment.id)
	final_file_name = '%s%s' % (file_key, ext)

	return os.path.join('static', strftime("%Y/%m/", gmtime()), final_file_name)


#Model del perfil del usuario
class Comment(models.Model):
	#ForeignKeys
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	taged_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='taged_user', null=True	, blank=True)

	#Content Fiels
	comment = models.TextField()

	#Date Controls Fiels
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	#Keys de Referencia
	share = models.ForeignKey('self', on_delete=models.PROTECT, related_name='sharepost', null=True, blank=True)
	article = models.ForeignKey(Articulo, null=True, blank=True, on_delete=models.PROTECT)


	class Meta:
		ordering = ['-updated_at']


	def verify_like(self, user):
		try:
			like = self.likes_set.get(user=user)
		except:
			like = None

		return True if like else False


	def get_comment(self):
		t = self.comment

		#Anti XSS
		t = t.replace('&','&amp;')
		t = t.replace('<','&lt;')
		t = t.replace('>','&gt;')
		t = t.replace('\'','&#39;')
		t = t.replace('"','&quot;')


		links = re.findall('(http\\:\\/\\/[^ ]+|https\\:\\/\\/[^ ]+)', t)
		for link in links:
			if 'https://www.youtube.com' in link:
				t = t.replace(link, '<div style="text-align:center;"><iframe width="560" height="315" src="%s" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>' % link.replace('watch?v=', 'embed/'))
			elif 'https://youtu.be/' in link:
				t = t.replace(link, '<div style="text-align:center;"><iframe width="560" height="315" src="%s" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>' % link.replace('https://youtu.be/', 'https://www.youtube.com/embed/'))
			else:
				t = t.replace(link, '<a href="%s">%s</a>' % (link, link))

		hashtags = re.findall('#[a-zA-Z0-9][a-zA-Z0-9]*', t)
		for hashtag in hashtags:
			t = t.replace(hashtag, '<a href="/comments/search?%s">%s</a>' % (urlencode({'tag':hashtag[1:]}), hashtag))

		menciones = re.findall('\\@[a-zA-Z0-9.]+', t)
		for mencion in menciones:
			t = t.replace(mencion, '<a href="/u/%s">%s</a>' % (mencion[1:], mencion))

		return t



class CommentImages(models.Model):
	#ForeignKeys
	comment = models.ForeignKey(Comment, on_delete=models.PROTECT)

	#Content Fiels
	image = models.ImageField(upload_to=upload_to_static_file, null=True, blank=True)

	#Date Controls Fiels
	created_at = models.DateTimeField(auto_now_add=True)


#Model del perfil del usuario
class Subcomment(models.Model):
	#ForeignKeys
	comment = models.ForeignKey(Comment, on_delete=models.PROTECT)
	user = models.ForeignKey(User, on_delete=models.PROTECT)

	#Content Fiels
	comment_text = models.TextField()

	#Date Controls Fiels
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


#Model del perfil del usuario
class Likes(models.Model):
	#ForeignKeys
	comment = models.ForeignKey(Comment, on_delete=models.PROTECT)
	user = models.ForeignKey(User, on_delete=models.PROTECT)
	created_at = models.DateTimeField(auto_now_add=True)