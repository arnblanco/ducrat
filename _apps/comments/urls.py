from django.urls import path
from django.contrib.auth import views as auth_views
from _apps.comments import views

app_name = 'comments'

urlpatterns = [
	path('search', views.tag_serach, name='tag_serach'),
]