from django.urls import path
from _apps.website import views

app_name = 'website'

urlpatterns = [
	path('abaut', views.abaut_page, name='abaut_page'),
]