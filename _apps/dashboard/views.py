# -*- coding: utf-8 -*-
import json
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect, csrf_exempt


"""
Dashboard
Esta pantalla ara el trabajo de pantalla principal del sitio web, contendra la siguiente informacion:
* Sistema de Autenticacion de Usuarios
* Links Informativos
* Pagina principal de usuarios autenticados
"""
def dashboard_homepge(request, aut_users='dashboard/dashboard_homepge.html', wellcome_users='website/wellcome.html'):
	user = request.user

	if user.is_authenticated:
		return render(request, aut_users, {})
	else:
		return render(request, wellcome_users, {})