# -*- coding: utf-8 -*-
from django.db import models

from django.utils.translation import ugettext as _


#Regiones soportadas
class Regions(models.Model):
	codigo = models.CharField(max_length=2, null=False, blank=False)
	region_name = models.CharField(max_length=64, null=False, blank=False)