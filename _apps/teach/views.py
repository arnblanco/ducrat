# -*- coding: utf-8 -*-
import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, JsonResponse
from django.utils.translation import ugettext as _
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_protect, csrf_exempt

from _components.decorators import verify_user_is_teacher

@csrf_protect
@login_required()
@verify_user_is_teacher
def home_teach(request, response_template='courses/home_courses.html'):
	user = request.user
	
	context = {}
	return render(request, response_template, context)


@csrf_protect
@login_required()
def teach_start(request, response_template='teach/start_teaching.html'):
	user = request.user
	
	context = {}
	return render(request, response_template, context)