from django.urls import path
from _apps.teach import views

app_name = 'teach'

urlpatterns = [
	path('', views.home_teach, name='home_teach'),
	path('start', views.teach_start, name='teach_start'),
]