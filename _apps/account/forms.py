# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm, Textarea
from django.utils.translation import ugettext as _

from django.contrib.auth.models import User
from .models import Profile


#Informacion del usuario, datos generales de la cuenta
class user_single_data(forms.ModelForm):
    first_name = forms.CharField(min_length=3)
    last_name = forms.CharField(min_length=3)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email']
        labels = {
            'first_name': _('Nombre'),
            'last_name': _('Apellido'),
            'username': _('Nombre de Usuario'),
            'email': _('Correo Electrónico')
        }
        help_texts = {
            'username': _('Nombre de usuario y de perfil, este es el nombre por el cual otros usuarios podran identificarte, puede contener letras, numeros y puntos')
        }

    def __init__(self, *args, **kwargs):
        super(user_single_data, self).__init__(*args, **kwargs)

        for key in self.fields:
            self.fields[key].required = True


#Informacion del perfil del usuario
class edit_user_perfil(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['biografia', 'gender', 'lenguaje', 'region', 'timezone']
        widgets = {
            'biografia': Textarea(attrs={'rows': 4}),
        }
        labels = {
            'biografia': _('Cuentanos algo sobre ti'),
            'gender': _('Genero'),
            'lenguaje': _('Lenguaje'),
            'region': _('Región'),
            'timezone': _('Zona Horaria')
        }
        help_texts = {
            'lenguaje': _('Idioma de preferencia para el contenido de la aplicación.'),
            'region': _('Nos permite ropicalizar y mostrar contenido mas apegado a tus gustos.'),
            'timezone': _('Nos permite ser mas certero en teimpos y fechas de eventos.')
        }

    def __init__(self, *args, **kwargs):
        super(edit_user_perfil, self).__init__(*args, **kwargs)

        for key in self.fields:
            self.fields[key].required = True 