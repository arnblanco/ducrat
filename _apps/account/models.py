# -*- coding: utf-8 -*-
import base64
import hashlib
import os
import pytz

from time import gmtime, strftime
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext as _
from django.conf import settings
from social_django.models import UserSocialAuth

from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute, NumberAttribute, UnicodeSetAttribute, UTCDateTimeAttribute, MapAttribute, ListAttribute
)

from _apps.system.models import Regions


def upload_to_profile_static(instance, filename):
	name, ext = os.path.splitext(filename)
	final_file_name = '%s-%s%s.%s' % (name, instance.user.id, strftime("%Y%m%y", gmtime()), ext)

	return os.path.join('profiles', strftime("%Y/%m/", gmtime()), final_file_name)


#Model del perfil del usuario
class Profile(models.Model):
    GENDER_CHOICES = (
    	('i', _('Indefinido')),
    	('f', _('Femenino')),
    	('m', _('Masculino'))
    )

    user = models.OneToOneField(User, primary_key = True, on_delete=models.CASCADE)
    picture = models.ImageField(upload_to=upload_to_profile_static, null=True, blank=True)
    cover = models.ImageField(upload_to=upload_to_profile_static, null=True, blank=True)
    gender = models.CharField(max_length=1, null=False, blank=False, default='i', choices=GENDER_CHOICES)
    lenguaje = models.CharField(max_length=2, null=False, blank=False, default='en', choices=settings.LANGUAGES)
    region = models.ForeignKey(Regions, on_delete=models.CASCADE, null=True, blank=True, choices=[(reg.id, reg.region_name) for reg in Regions.objects.all()])
    timezone = models.CharField(max_length=64, null=False, blank=False, default='America/New_York', choices=[(x, x) for x in pytz.common_timezones])
    biografia = models.TextField(null=True, blank=True, default=_("Hey, ahora estoy en DuCrat"))
    mail_verified = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)


    #Carga la fotografia del usuario, ya sea la local o la social
    def get_avatar(self):
    	if self.picture:
    		return self.picture.url
    	else:
    		try:
    			social = UserSocialAuth.objects.get(user = self.user)
    		except UserSocialAuth.DoesNotExist:
    			social = None

    		if social:
    			if social.provider=='facebook':
    				return 'https://graph.facebook.com/%s/picture?height=500&type=large&width=500' % (social.uid)
    			else:
    				return self.picture.url
    		else:
    			return '/static/assets/images/avatar.png'


    #Carga la fotografia del usuario, ya sea la local o la social
    def get_cover(self):
        if self.cover:
            return self.cover.url
        else:
            return '/static/images/social/img1.jpg'


    #Verificar si el usuario cargado sigue a otro usuario
    def verify_follow(self, per):
        if(self.user != per):
            try:
                fw = self.user.follower.get(follow=per)
            except:
                fw = None

            if fw:
                return True
            else:
                return False
        return False

    #Verificar correo electronico del usaurio
    def verify_my_email(self):
        return True
    

    def get_my_key(self):
        user_key = 'user-%s' % self.user.id
        user_string_key = base64.urlsafe_b64encode(hashlib.sha256(user_key.encode('utf8', 'ignore')).digest())
        return user_string_key.decode('utf8')


    #Cargar el total de personas seguidas por este usuario
    def get_follows(self):
        follows = self.user.follower.all()
        return len(follows)

    #Cargar el total de personas que siguen a este usuario
    def get_followers(self):
        followers = self.user.follow.all()
        return len(followers)


#Modelo de seguimiento de usuarios
class Follow(models.Model):
    follower = models.ForeignKey(User, related_name='follower', on_delete=models.CASCADE) #El que sigue
    follow = models.ForeignKey(User, related_name='follow', on_delete=models.CASCADE) #Al que se sigue
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('follow', 'follower')