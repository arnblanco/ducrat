from django.urls import path
from _apps.account import views

app_name = 'account'

urlpatterns = [
	path('<username>', views.user_perfil, name='user_perfil'),
	path('<username>/post', views.user_post, name='user_post'),
]