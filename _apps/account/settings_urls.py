from django.urls import path
from _apps.account import views

app_name = 'account'

urlpatterns = [
	path('account', views.settings_informacion_general, name='informacion_general'),
	path('profile', views.settings_informacion_perfil, name='informacion_perfil'),
	path('academic', views.settings_informacion_academico, name='informacion_academico'),
	path('password', views.settings_informacion_perfil, name='informacion_password'),
	path('security', views.settings_informacion_perfil, name='informacion_seguridad'),
	path('notifications', views.settings_informacion_perfil, name='informacion_notificaciones'),
	path('billing', views.settings_informacion_perfil, name='informacion_facturacion'),
	path('blacklist', views.settings_informacion_perfil, name='informacion_blacklist'),
]