# Generated by Django 2.0.6 on 2018-06-26 18:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0003_profile_mail_verified'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='follow',
            unique_together=set(),
        ),
        migrations.RemoveField(
            model_name='follow',
            name='follow',
        ),
        migrations.RemoveField(
            model_name='follow',
            name='follower',
        ),
        migrations.DeleteModel(
            name='Follow',
        ),
    ]
