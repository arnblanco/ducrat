from django.urls import path
from django.contrib.auth import views as auth_views
from _apps.account import views

app_name = 'account'

urlpatterns = [
	path('logout', auth_views.logout, {'next_page': '/'}, name='auth_user_salir'),
]