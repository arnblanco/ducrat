# -*- coding: utf-8 -*-
import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, JsonResponse
from django.utils.translation import ugettext as _
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_protect, csrf_exempt

from .forms import (
	user_single_data,
	edit_user_perfil
)



#--------------------------------------------OPCIONES DEL PERFIL DEL USUARIO----------------------------------#
#Pefil del comercio
@csrf_protect
@login_required()
def user_perfil(request, username, response_template='account/profile/user-perfil.html'):
	user = request.user
	per = get_object_or_404(User, username=username)
	
	#Verificar si el usuario ya se dio follow
	per.is_follow = user.profile.verify_follow(per)
	
	context = {'per':per}
	return render(request, response_template, context)



@csrf_protect
@login_required()
def user_post(request, username, response_template='account/profile/user-post.html'):
	user = request.user
	per = get_object_or_404(User, username=username)
	
	context = {'per':per}
	return render(request, response_template, context)



#--------------------------------------------OPCIONES CONFIGURACION DE LA CUENTA----------------------------------#
#Informacion general de la Cuenta
@csrf_protect
@login_required()
def settings_informacion_general(request, response_template='account/settings/informacion_general.html', user_form=user_single_data):
	user = request.user
	
	form = user_form(instance=user)

	context = {'form':form}
	return render(request, response_template, context)


#Informacion del Perfil
@csrf_protect
@login_required()
def settings_informacion_perfil(request, response_template='account/settings/informacion_perfil.html', user_form=edit_user_perfil):
	user = request.user

	form = user_form(instance=user.profile)

	context = {'form':form}
	return render(request, response_template, context)


#Informacion Academica principal del Usuario
@csrf_protect
@login_required()
def settings_informacion_academico(request, response_template='account/settings/informacion_academico.html', user_form=edit_user_perfil):
	user = request.user
	form = user_form(instance=user)

	context = {'form':form}
	return render(request, response_template, context)