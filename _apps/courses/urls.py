from django.urls import path
from _apps.courses import views

app_name = 'courses'

urlpatterns = [
	path('', views.home_courses, name='home_courses'),
]