# -*- coding: utf-8 -*-
import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, JsonResponse
from django.utils.translation import ugettext as _
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_protect, csrf_exempt



@csrf_protect
@login_required()
def home_courses(request, response_template='courses/home_courses.html'):
	user = request.user
	
	context = {}
	return render(request, response_template, context)
