from django.urls import path
from _apps.articles import views

app_name = 'articles'

urlpatterns = [
	path('', views.home_articles, name='home_articles'),
	path('new', views.articles_creator, name='articles_creator'),
	path('<int:aid>-<slug:slug>', views.article_details, name='article_details'),
]