# -*- coding: utf-8 -*-
import base64
import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.files.base import ContentFile
from django.http import HttpResponse, JsonResponse
from django.utils.translation import ugettext as _
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_protect, csrf_exempt

from .models import Articulo

@csrf_protect
@login_required()
def home_articles(request, response_template='articles/home_articles.html'):
	user = request.user

	context = {}
	return render(request, response_template, context)


@csrf_protect
@login_required()
def articles_creator(request, response_template='articles/articles_creator.html'):
	user = request.user
	
	if request.POST:
		try:
			cover = request.POST['article-cover']
		except:
			cover = ''

		try:
			title = request.POST['title']
		except:
			title = ''

		try:
			comment = request.POST['comment']
		except:
			comment = ''

		if(not title == '' and not comment == ''):
			articulo = user.articulo_set.create(
				title = title,
				content = comment
			)

			if not cover == '':
				exdata, imgstr = cover.split(';base64,')
				ext = exdata.split('/')[-1]
				image_name = '%s-article.%s' % (articulo.id, ext)
				articulo.cover.save(image_name, ContentFile(base64.b64decode(imgstr)))
				articulo.save()


	context = {}
	return render(request, response_template, context)


@csrf_protect
@login_required()
def article_details(request, aid, slug, response_template='articles/details_article.html'):
	user = request.user

	articulo = get_object_or_404(Articulo, id=aid, slug=slug)

	context = {'articulo':articulo}
	return render(request, response_template, context)