# -*- coding: utf-8 -*-
import base64
import hashlib
import os
import pytz

from bs4 import BeautifulSoup

from time import gmtime, strftime
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext as _
from django.template.defaultfilters import slugify



def upload_to_article_static(instance, filename):
	name, ext = os.path.splitext(filename)
	final_file_name = '%s-%s%s.%s' % (name, instance.user.id, strftime("%Y%m%y", gmtime()), ext)

	return os.path.join('articles', strftime("%Y/%m/", gmtime()), final_file_name)


#Model del perfil del usuario
class Articulo(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    cover = models.ImageField(upload_to=upload_to_article_static, null=True, blank=True)
    title = models.CharField(max_length=64, null=False, blank=False)
    slug = models.SlugField(max_length=128, null=True, blank=True)
    content = models.TextField(null=False, blank=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-updated_at']

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Articulo, self).save(*args, **kwargs)


    def get_conent(self):
        return self.content


    def get_short_content(self):
        cleantext = BeautifulSoup(self.content, "html.parser").text
        return '%s...' % cleantext[:200]
