import base64
import hashlib

from social_core.backends.facebook import FacebookOAuth2
from social_core.backends.twitter import TwitterOAuth
from social_core.backends.google import GoogleOAuth2

from urllib.request import urlopen

from django.conf import settings
from django.core.files.base import ContentFile
from django.template.defaultfilters import slugify

from _apps.account.models import Profile

def new_users_handler(backend, details, response, user=None, is_new=False, *args, **kwargs) :
    if user is None:
        return False

    if is_new:
        profile = Profile.objects.create(
            user=user,
            mail_verified=True
        )
        profile.save()

        if "id" in response:
            url = None
            cover = None
            print(response)

            if isinstance(backend, FacebookOAuth2):
                print(response)
            elif isinstance(backend, TwitterOAuth):
                if response['description'] != '':
                    profile.biografia = response['description'] if 'description' in response else ''

                if response['profile_image_url'] != '':
                    url = response['profile_image_url'].replace('_normal', '')

                if response['profile_banner_url'] != '':
                    cover = response['profile_banner_url']

                if response['lang'] != '':
                    profile.lenguaje = response['lang'] if any(response['lang'] in lng for lng in settings.LANGUAGES) else 'en'

            elif isinstance(backend, GoogleOAuth2):
                if ('tagline' in response and response['tagline'] != ''):
                    profile.biografia = response['tagline']

                if response['image'].get('url') is not None:
                    url = response['image'].get('url').replace('sz=50', 'sz=500')

                if ('language' in response and response['language'] != ''):
                    profile.lenguaje = response['language'] if any(response['language'] in lng for lng in settings.LANGUAGES) else 'en'

                if 'gender' in response:
                    if response['gender'] == 'male':
                        profile.gender = 'm'
                    elif response['gender'] == 'female':
                        profile.gender = 'f'
                    else:
                        profile.gender = 'i'

            if url:
                avatar = urlopen(url)
                profile.picture.save('profile_image.jpg', ContentFile(avatar.read()))

            if cover:
                fucover = urlopen(cover)
                profile.cover.save('cover_image.jpg', ContentFile(fucover.read()))
            

            profile.save()
    return False