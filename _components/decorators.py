from django.shortcuts import redirect


def verify_user_is_teacher(function):
	def wrap(request, *args, **kwargs):
		user = request.user
		profile = user.profile

		if profile.is_teacher:
			return True
		else:
			return redirect('urls_teach:teach_start')

	wrap.__doc__ = function.__doc__
	wrap.__name__ = function.__name__
	return wrap
